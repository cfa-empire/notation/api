let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');

const flash = require('express-flash');
const session = require('express-session');
const cors = require('cors')

require('dotenv').config();

let app = express();
let router = require('./router');

app.use(cors());

//middleware
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(flash())
app.use(session({
    secret: process.env.SESSION_SECRET_KEY,
    resave: false,
    saveUninitialized: false,
}))

app.use('/api', router)

//connection base de données
mongoose.connect(process.env.MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
}, function(err) {
    if (err) {
        return console.log('MONGODB Error:', err);
    }
    console.log('Successfully connected to the Mongo Database !');
});

//définition port d'écoute
var port = process.env.PORT || 3000;

app.listen(port, function() {
    console.log('listening on port', port);
})