let Admin = require('../models/admin');
const authController = require('./auth');

exports.register = async function (req, res) {
  const email = req.body.email;  
  const password = req.body.password;  
  const firstname = req.body.firstname; 
  const lastname = req.body.lastname;  

  try {
    let oldAdmin = await Admin.findOne({email});

    if (oldAdmin) {
      return res.status(400).send("l'email existe déjà !");
    }

    let admin = new Admin({
      email,
      password: authController.hashPassword(password),
      firstname,
      lastname
    });

    let savedAdmin = await admin.save();

    savedAdmin.password = '';

    res.json({
      user: savedAdmin
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};


exports.list = async function (req, res) {
  try {
    let admins = await Admin.find().select('-password -__v');

    res.json({
      users: admins
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};