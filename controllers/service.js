let Service = require('../models/service');
let QRCode = require('qrcode');

exports.create = async function (req, res) {
  const name = req.body.name;  
  const description = req.body.description;  
  const userId = req.body.userId;
  const contact = req.body.contact;
  
  try {
    
    let service = new Service({
      name,
      description,
      userId,
      contact
      //qrcode
    });

    let savedService = await service.save();

    const qrcode = await generateQrCode(savedService._id);

    savedService.qrcode = qrcode;
    let finalService = await savedService.save();

    res.json({
      service: finalService
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function (req, res) {
  const id = req.body.id;;
  try{
    let foundService = await Service.findOne({_id: id});

    foundService._id = req.body.id || foundService._id;
    foundService.name = req.body.name || foundService.name;
    //foundService.description = req.body.description || foundService.description;
    foundService.contact = req.body.contact || foundService.contact;
    foundService.qrcode = await generateQrCode(foundService._id);
    
    let updatedService = await foundService.save();
    
    res.json({
      service: updatedService
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

function generateQrCode(id) {
  var segs = [
    { data: id, mode: 'byte' }
  ]
  
  return QRCode.toDataURL(id.toString())
};


exports.listAll = async function (req, res) {
  try {
    let services = await Service.find().select('-__v');
    res.json({
      services: services
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.listByUser = async function (req, res) {
  const userId = req.query.userId;
  try {
    let services = await Service.find({userId: userId}).select('-__v -password -userId ');
    res.json({
      services: services
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.getById = async function (req, res) {
  const id = req.query.id;
  try {
    let service = await Service.findOne({_id: id}).select('-__v');
    res.json({
      service: service
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function (req, res, next) {
  const id = req.params.id;
  try {
    await Service.findByIdAndRemove(id, req.body);
    res.send("Service was deleted.");
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  } 
};

exports.generateQrCode = async function(req, res, next){
  let qrcode;
  const service_id = req.body.service_id;

  const param1 = req.body.name;
  const param2 = req.body.contact;
  const param3 = req.body.description;

  var segs = [
    { data: param1, mode: 'alphanumeric' },
    { data: param2, mode: 'numeric' },
    { data: param3, mode: 'byte' }
  ]

  QRCode.toDataURL(segs)
    .then(url => {
      qrcode = url
    })
    .catch(err => {
      console.error(err)
    })

  try {
    let updatedService = await Service.findByIdAndUpdate(service_id, { qrcode });
    res.json({
        serviceUpdate: updatedService,
        serviceQrcode: qrcode
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.uploadCover = async function(req, res, next) {
  const id = req.params.id;

  try{
    let foundService = await Service.findOne({_id: id});
    
    foundService.cover = req.file ? req.file.location : '';

    let updatedService = await foundService.save();
    
    res.json({
      service: updatedService 
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.uploadLogo = async function(req, res, next) {
  const id = req.params.id;

  try{
    let foundService = await Service.findOne({_id: id});
    foundService.logo = req.file ? req.file.location : '';
    let updatedService = await foundService.save();

    res.json({
      service: updatedService 
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};
