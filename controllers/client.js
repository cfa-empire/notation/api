let Client = require('../models/client');
const uuidv4 = require('uuid/v4');

exports.create = async function(req, res) {
  const appId = uuidv4();

  try {
    let client = new Client({
      appId,
    });
    
    let savedClient = await client.save();
    
    res.json({
      client: savedClient
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function(req, res) {
  const appId = req.body.appId;
  const client_id = req.body.client_id
  try {
    let updatedClient = await Client.findByIdAndUpdate(client_id, { appId });
    
    res.json({
      user: updatedClient
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function(req, res) {
  const client_id = req.body.client_id
  try {
    let deletedClient = await Client.findByIdAndDelete(client_id);
    
    res.json({
      user: deletedClient
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};