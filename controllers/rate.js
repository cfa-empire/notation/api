let Rate = require('../models/rate');

exports.create = async function(req, res) {
  const note = req.body.note;
  const total = req.body.total;
  const clientId = req.body.clientId;
  const serviceId = req.body.serviceId;
  const commentaire = req.body.commentaire;
  const date = new Date();

  try {
    let rate = new Rate({
      note,
      total,
      commentaire,
      clientId,
      serviceId,
      date
    });

    let savedRate = await rate.save();

    res.json({
      rate: savedRate
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function(req, res) {
  const rate_id = req.params.id;
  const note = req.body.note;
  const total = req.body.total;
  const clientId = req.body.clientId;
  const serviceId = req.body.serviceId;
  const commentaire = req.body.commentaire;
  
  try {
    let updatedRate = await Rate.findByIdAndUpdate(rate_id, { note, total, clientId, serviceId, commentaire });
    
    res.json({
      rate: updatedRate
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function(req, res) {
  const rate_id = req.params.id;
  try {
    let deletedRate = await Rate.findByIdAndDelete(rate_id);
    res.json({
      rate: deletedRate
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.listByClient = async function(req, res) {
  const clientId = req.params.id;

  try {
    let rates = await Rate.find({ clientId })
    res.json({
      rates: rates
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.listAll = async function(req, res) {
  try {
    let rates = await Rate.find().select('-__v');
    res.json({
      rates: rates
    });
  } catch (err) {
    console.log(err); 
    res.status(400).send(err);
  }
};

exports.listByService = async function(req, res) {
  const serviceId = req.query.serviceId;
  try {
    let rates = await Rate.find({ serviceId })

    res.json({
      rates: rates
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};