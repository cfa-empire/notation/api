let User = require('../models/user');
const authController = require('./auth');
const uuidv4 = require('uuid/v4');

exports.register = async function (req, res) {
  const email = req.body.email;  
  const password = req.body.password;  
  const firstname = req.body.firstname; 
  const lastname = req.body.lastname;  
  const contact = req.body.contact;

  try {
    let oldUser = await User.findOne({email});

    if (oldUser) {
      return res.status(400).send("l'email existe déjà !");
    }

    let new_user = new User({
      email,
      password: authController.hashPassword(password),
      firstname,
      lastname,
      contact,
    });

    let savedUser = await new_user.save();

    authController.login(new_user, res);
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.loadMe = async function (req, res) {
  if (req.user) {
    res.json({
      user: req.user
    });
  } else {
    res.status(400).send('User not found'); 
  }
};

exports.list = async function (req, res) {
  try {
    let users = await User.find().select('-password -__v');

    res.json({
      users: users
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.update = async function (req, res) {
  const id = req.params.id;

  try {
    let foundUser =  await User.findOne({_id: id});

    foundUser.email = req.body.email || foundUser.email;
    foundUser.firstname = req.body.firstname || foundUser.firstname;
    foundUser.lastname = req.body.lastname || foundUser.lastname;
    foundUser.contact = req.body.contact || foundUser.contact;
  
    await foundUser.save();
    
    res.json({
      user: foundUser
    });
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.forgetPassword = async function (req, res) {
  try {
    let newUser = await User.findOne({email: req.body.email}).select('-password -__v');
    
    newUser.resetToken = uuidv4(); 
    
    await newUser.save();

    res.json({
      user: newUser
    });
   
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }

};

exports.resetPassword = async function (req, res) {
  const id = req.params.id;

  try {
    let foundUser = await User.findOne({_id: id});

    if(req.body.password){
      
      foundUser.password = authController.hashPassword(req.body.password);

      const updatedUser = await foundUser.save();
      
      res.json({
        user: updatedUser
      });
    } else {
      res.json({
        message: 'Pas de modification de password possible.'
      });   
    }        
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }
};

exports.delete = async function (req, res) {
  const id = req.params.id;
  try {
    await User.findByIdAndRemove(id);
    res.send("the user was deleted.");
  } catch (err) {
    console.log(err);
    res.status(400).send(err);
  }  
};