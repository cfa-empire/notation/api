let express = require('express');

let authController = require('./controllers/auth');
let adminController = require('./controllers/admin');
let clientController = require('./controllers/client')
let rateController = require('./controllers/rate')
let userController = require('./controllers/user');
let serviceController = require('./controllers/service');
let s3 = require('./services/s3');

const isLogged = authController.verifyLoggedUser;

module.exports = (function() {
  let router = express.Router();

  router.get('/admins/list', isLogged, adminController.list);
  router.post('/admins/login', authController.adminLogin);
  router.post('/admins/register', adminController.register);
  
  router.post('/clients/create', clientController.create);
  router.post('/clients/update', clientController.update);
  router.post('/clients/delete', clientController.delete);
  
  router.get('/rates/listAll', isLogged, rateController.listAll);
  router.get('/rates/service', isLogged, rateController.listByService);
  router.get('/rates/client/:id', isLogged, rateController.listByClient);
  router.post('/rates/create', rateController.create);
  router.put('/rates/:id', rateController.update);
  router.delete('/rates/:id', rateController.delete);

  router.post('/users/login', authController.userLogin);
  router.get('/users', isLogged, userController.loadMe);
  router.post('/users', userController.register);
  router.post('/users/forget-password', userController.forgetPassword);
  router.put('/users/:id', isLogged, userController.update);
  router.put('/users/:id/reset-password', isLogged, userController.resetPassword);
  router.delete('/users/:id', isLogged, userController.delete);

  router.get('/services/listAll', isLogged, serviceController.listAll);
  router.get('/services/listByUser', isLogged, serviceController.listByUser);
  router.get('/services/listById', serviceController.getById);
  
  router.post('/services/create', isLogged, serviceController.create);
  router.put('/services/update', isLogged, serviceController.update);
  router.post('/services/generate-qr-code', isLogged, serviceController.generateQrCode);
  router.post('/services/:id/upload-cover', isLogged, s3.upload.single('cover'), serviceController.uploadCover);
  router.post('/services/:id/upload-logo', isLogged, s3.upload.single('logo'), serviceController.uploadLogo);
  router.delete('/services/:id', isLogged, serviceController.delete);

  return router;
})(); 
