let mongoose = require('mongoose')

let RateSchema = mongoose.Schema({
  note: { type: Number, },
  total: { type: Number },
  commentaire: { type: String, },
  clientId: { type: String },
  serviceId: { type: String },
  date: { type: String, }
});

module.exports = mongoose.model('Rate', RateSchema);