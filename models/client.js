let mongoose = require('mongoose') 

let ClientSchema = mongoose.Schema({
  appId: {type: String, unique: true},
});

module.exports = mongoose.model('Client', ClientSchema);