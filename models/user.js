let mongoose = require('mongoose') 

let UserSchema = mongoose.Schema({
  email: {type: String, unique: true},
  password: {type: String},
  firstname: {type: String},
  lastname: {type: String},
  contact: {type: String},
  resetToken: {type: String}
});

module.exports = mongoose.model('User', UserSchema);