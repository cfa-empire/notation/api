let mongoose = require('mongoose') 

let AdminSchema = mongoose.Schema({
  email: {type: String, unique: true},
  password: {type: String},
  firstname: {type: String},
  lastname: {type: String},
  type: {type: String, default: 'admin'}
});

module.exports = mongoose.model('Admin', AdminSchema);