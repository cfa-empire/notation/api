let mongoose = require('mongoose') 

let ServiceSchema = mongoose.Schema({
  name: {type: String}, 
  description: {type: String},
  qrcode: {type: String},
  userId: {type: String},
  cover: {type: String},
  logo: {type: String},
  contact: {type: Number}
});

module.exports = mongoose.model('Service', ServiceSchema);